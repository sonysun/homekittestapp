//
//  AppDelegate.h
//  HomeKitTestApp
//
//  Created by Nadein on 4/4/16.
//  Copyright © 2016 Alex Nadein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

