//
//  AppDelegate.m
//  HomeKitTestApp
//
//  Created by Nadein on 4/4/16.
//  Copyright © 2016 Alex Nadein. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self configureWindow];
    
    return YES;
}

-(void)configureWindow
{
//    [self setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
//    [[self window] setRootViewController:];
//    [[self window] setBackgroundColor:[UIColor whiteColor]];
//    [[self window] makeKeyAndVisible];
}


@end
